Place the hacked exe in the game directory alongside the original `lamune.exe`. Run the game with the hacked version instead of the original exe.

**Note**: While this exe can be used to run the game, it's an incomplete version used as an intermediary for the full translation of the exe not intended to be played. It's better to use the full English translated version.